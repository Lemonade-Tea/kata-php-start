<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class TestsFizzBuzz extends TestCase
{

    public function assertEvaluate($expected, $value)
    {
        $fizzBuzz = new FizzBuzz();
        $this->assertEquals($expected, $fizzBuzz->evaluate($value));
    }

    public function testShouldReturn1for1()
    {
        $this->assertEvaluate(1, 1);
    }


    /**
     * @dataProvider provideMultiplesOfOnly3
     */
    public function testShouldReturnFizzforMultiplesOfOnly3($number)
    {
        $this->assertEvaluate('Fizz', $number);
    }

    /**
     * @dataProvider provideMultiplesOfOnly5
     */
    public function testShouldReturnBuzzforMultiplesOfOnly5($number)
    {
        $this->assertEvaluate('Buzz', $number);
    }

    /**
     * @dataProvider provideMultiplesOfBoth3And5
     */
    public function testShouldReturnFizzBuzzForMultiplesOfBoth3And5($number)
    {
        $this->assertEvaluate('FizzBuzz', $number);
    }

    /**
     * @dataProvider provideNumbersNotMultiplesOf3Or5
     */
    public function testShouldReturnNumberWhenNumberIsNotMultipleOf3Or5($number)
    {
        $this->assertEvaluate($number, $number);
    }

    public function provideMultiplesOfOnly3()
    {
        return [[3], [6], [9]];
    }

    public function provideMultiplesOfOnly5()
    {
        return [[5], [10], [20]];
    }

    public function provideMultiplesOfBoth3And5()
    {
        return [[15], [30], [45]];
    }

    public function provideNumbersNotMultiplesOf3Or5()
    {
        return [[1], [2], [4], [7], [11], [19]];
    }
}
