<?php

class FizzBuzz
{
    public function evaluate(Int $number = 1)
    {
        if ($this->isDividableBy5($number) & $this->isDividableBy3($number)) {
            return 'FizzBuzz';
        } else if ($this->isDividableBy3($number)) {
            return 'Fizz';
        } else if ($this->isDividableBy5($number)) {
            return 'Buzz';
        } else {
            return $number;
        }
    }

    public function isDividableBy3(Int $number)
    {
        return ($number % 3 == 0);
    }

    public function isDividableBy5(Int $number)
    {
        return ($number % 5 == 0);
    }
}
